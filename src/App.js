import 'antd/dist/antd.css';
import "./App.css";
import Navbar from "../src/components/Navbar";
import Footer from "./components/Footer";
import Home from "../src/components/Home";
import Menu from "./pages/Menu";
import About from "../src/components/About";
import Contact from "../src/components/Contact";
import Feedback from './components/Feedback';
import Service_request from "./pages/Service_request"
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Service from "./pages/Service";
import DemoInstallation from "./pages/DemoInstallation";
import Review from './components/Review';
import UserAuthenticationContextProvider from './pages/Context/UserDetailsContext'




function App() {

  return (
    <UserAuthenticationContextProvider>
      <div className="App">



        <Router>

          <Navbar />
          <Routes>
            <Route path="/" exact element={<Home />} />
            <Route path="/menu" exact element={<Menu />} />
            <Route path="/about" exact element={<About />} />
            <Route path="/contact" exact element={<Contact />} />
            <Route path="/feedback" exact element={<Feedback />} />
            {/* <Route path="/service" exact element={<Service />} /> */}
            <Route path="/service" exact element={<Service_request />} />
            <Route path="/service/Feedback" exact element={<Feedback />} />
            <Route path="/service/DemoInstallation" exact element={<DemoInstallation />} />
            <Route path="/Review" exact element={<Review />} />

          </Routes>
          <Footer />
        </Router>

      </div>
    </UserAuthenticationContextProvider>
  );
}

export default App;
