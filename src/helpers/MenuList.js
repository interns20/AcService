import BlueStar from "../assets/BlueStar.png";
import Samsung from "../assets/samsung.png";
import LGAc from "../assets/LGAc.jpg";
import whirpool from "../assets/Whirlpool.png";
import Llyod from "../assets/Lloyd.png";
import OGeneral from "../assets/OGeneral.png";

export const MenuList = [
  {
    name: "BlueStar",
    image: BlueStar,
    price: 30000,
    Power: 61,
    Temperature: 52,
    Refrigerant: 32,

  },
  {
    name: "Samsung",
    image: Samsung,
    price: 16000.99,
    power: 600.91,
    Temperature: 50,
    Refrigerant: 32,




  },
  {
    name: "LG",
    image: LGAc,
    price: 43000,
    power: 600.91,
    Temperature: 49,
    Refrigerant: 12,

  },
  {
    name: "Whirpool",
    image: whirpool,
    price: 17000.99,
    power: 600.91,
    Temperature: 52,
    Refrigerant: 52,

  },
  {
    name: "Lloyd",
    image: Llyod,
    price: 18000.99,
    power: 600.91,
    Temperature: 52,
    Refrigerant: 32,

  },
  {
    name: "Haier",
    image: OGeneral,
    price: 45000.99,
    power: 579.91,
    Temperature: 45,
    Refrigerant: 30,

  },
];