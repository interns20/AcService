import { useState } from "react";
import './Feedback.css';
import { FaStar } from "react-icons/fa";
import { message } from 'antd'
import { } from '../'
import Review from "./Review";

const colors = {
  orange: "#FFBA5A",
  grey: "#a9a9a9"

};



function Feedback() {
  const [currentValue, setCurrentValue] = useState(0);
  const [hoverValue, setHoverValue] = useState(undefined);
  const [feedback, setFeedback] = useState("")
  const PRIMARY_API = "https://pnre7lwmvk.execute-api.ap-south-1.amazonaws.com/dev"
  const stars = Array(5).fill(0)
  const handleClick = value => {
    setCurrentValue(value)
  }

  const handleMouseOver = newHoverValue => {
    setHoverValue(newHoverValue)
  };

  const handleMouseLeave = () => {
    setHoverValue(undefined)
  }
  const onFinish = () => {
    if (!feedback) {
      message.error("feedback canot be empty")
      return
    }
    else {
      var adminVerifyRaw = JSON.stringify({ "appName": "INTERNS_APP_DEV", "modName": "CREATE_APPOINTMENT", attributeName: "feedback", attributeValue: [{ feedback: feedback, ratingss: currentValue }] })

      var adminVerifyRequestOptions = {

        method: 'POST',

        headers: { "Content-Type": "application/json" },

        body: adminVerifyRaw,

        redirect: 'follow'

      };

      fetch(`${PRIMARY_API}/general`, adminVerifyRequestOptions)

        .then(response => response.json())

        .then(result => {
          console.log(result)
          if (result.statusCode === 200) {
            setFeedback("")
            message.success("Feedback submitted successfully")
          }
        })
        .catch(err => console.log(err))
    };


  }





  return (
   <div>
      <div style={styles.container}>
      <h2>  Ratings </h2>
      <div style={styles.stars}>
        {stars.map((_, index) => {
          return (
            <FaStar
              key={index}
              size={24}
              onClick={() => handleClick(index + 1)}
              onMouseOver={() => handleMouseOver(index + 1)}
              onMouseLeave={handleMouseLeave}

              color={(hoverValue || currentValue) > index ? colors.orange : colors.grey}
              style={{
                marginRight: 10,
                cursor: "pointer"
              }}
            />
          )
        })}
      </div>
      <textarea
        value={feedback}
        placeholder="What's your experience?"
        onChange={(e) => setFeedback(e.target.value)}
        style={styles.textarea}
      />

      <button
        style={styles.button}
        onClick={() => onFinish()}
      >
        Submit
      </button>
     

    </div>
    <div><Review /></div>
   </div>

  );
};


const styles = {
  container: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  stars: {
    display: "flex",
    flexDirection: "row",
  },
  textarea: {
    border: "1px solid #a9a9a9",
    borderRadius: 5,
    padding: 10,
    margin: "20px 0",
    minHeight: 100,
    width: 300
  },
  button: {
    border: "1px solid #a9a9a9",
    borderRadius: 5,
    width: 300,
    padding: 10,
  }


};




export default Feedback;

