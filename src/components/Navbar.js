import React, { useState } from "react";
import Logo from "../assets/ac.png";
import { Link } from "react-router-dom";
import ReorderIcon from "@material-ui/icons/Reorder";
import "../styles/Navbar.css";

function Navbar() {
  const [openLinks, setOpenLinks] = useState(false);

  const toggleNavbar = () => {
    setOpenLinks(!openLinks);
  };
  return (
    <div className="navbar">
      <div className="leftSide" id={openLinks ? "open" : "close"}>
        <img src={Logo} />

        <h1 className="log">AIRorzo</h1>

        <div className="hiddenLinks">
          <Link to="/"> Home </Link>
          <Link to="/about"> About Us </Link>
          <Link to="/contact"> Contact Us </Link>
          <Link to="/Feedback">Feedback</Link>
        </div>
      </div>
      <div className="rightSide">

        <Link to="/"> Home </Link>

        <Link to="/about"> About Us</Link>
        <Link to="/contact"> Contact Us </Link>
        <Link to="/Feedback">Feedback</Link>
        <button onClick={toggleNavbar}>
          <ReorderIcon />
        </button>
      </div>
    </div>
  );
}

export default Navbar;