import React from "react";

import "../styles/About.css";
function About() {
    return (
        <div className="about">
            <div
                className="aboutTop"

            ></div>
            <div className="aboutBottom">
                <h1> ABOUT US</h1>
                <p>
                    We at Primo offer both standard and bespoke services and support to match your requirements by providing you with cost-effective and sustainable solutions. Backed by 3 years of extensive experience and supported by an in-house team of qualified, proactive, customer-focused technicians and engineers from diverse backgrounds and cultures, we are well-positioned to match expertise with client requirements of any complexity and also maintain control of the quality of service we provide. As our client, you can benefit from a consultative approach, greater control, reduced costs, improved quality, and multi-tasking skills. Primo has witnessed exponential growth and is today the company of choice for businesses and individual property owners who wish to outsource their building cleaning and maintenance work to a reliable and responsible partner.
                </p>
            </div>
        </div>
    );
}

export default About;