import React from "react";
import Call from "@material-ui/icons/Call";
import LocationOn from "@material-ui/icons/LocationOn";
import ContactMailSharp from "@material-ui/icons/Mail";
import { Form, Input, message, Button } from 'antd'
// import Ac from "./bg3.jpg";


import "../styles/Contact.css";

const PRIMARY_API = "https://pnre7lwmvk.execute-api.ap-south-1.amazonaws.com/dev"

function Contact() {
    const [form] = Form.useForm();
    const { TextArea } = Input;
    const onFinish = (values) => {
        form.resetFields()
        console.log(values)
        var adminVerifyRaw = JSON.stringify({ "appName": "INTERNS_APP_DEV", "modName": "CREATE_APPOINTMENT", attributeName: "contact", attributeValue: [values] })

        var adminVerifyRequestOptions = {

            method: 'POST',

            headers: { "Content-Type": "application/json" },

            body: adminVerifyRaw,

            redirect: 'follow'

        };

        fetch(`${PRIMARY_API}/general`, adminVerifyRequestOptions)

            .then(response => response.json())

            .then(result => {
                console.log(result)
                if (result.statusCode === 200) {
                    message.success("Contact submitted successfully")
                }
            })
            .catch(err => console.log(err))
    };
    return (
        <div className="contact" >

            <div className="rightSide">
                <h3> Contact Us</h3>
                {/* <h5>We love questions and feedback-and we're always happy to help! </h5> */}
                <div className="contactus-container">
                    <div className="col-6">
                        <Form form={form} onFinish={onFinish} id="contact-form">

                            <h1 align="left"> Contact Us</h1>
                            <label align="left" htmlFor="name"><h5>Name</h5></label>
                            <Form.Item name="name" rules={[{ required: true, message: 'Please enter your name' }]}>
                                <Input name="name" placeholder="Enter full name..." type="text" />
                            </Form.Item>
                            <label align="left" htmlFor="email"><h5>Email</h5></label>
                            <Form.Item name="email" rules={[{ required: true, message: 'Please enter your email' }]}>
                                <Input name="email" placeholder="Enter email..." type="email" />
                            </Form.Item>
                            <label align="left" htmlFor="message"><h5>Message</h5></label>
                            <Form.Item name="message" rules={[{ required: true, message: 'Please enter the message' }]}>
                                <TextArea
                                    rows="2"
                                    placeholder="Enter message..."
                                    name="message"
                                ></TextArea>
                            </Form.Item>
                            <Form.Item>
                                <Button htmlType="submit"> Submit</Button>
                            </Form.Item>
                        </Form>
                    </div>
                    <div className="col-7">
                        <h1> Contact Info</h1>
                        <h5> <div><Call />+91-9087656467</div></h5>
                        <h5><div><LocationOn />2nd floor, pammam, </div></h5>
                        <h5><div>Marthadam 629160</div></h5>
                        <h5><div><ContactMailSharp />airorzo@gmail.com</div></h5>
                    </div></div>
            </div>
        </div>
    );
}

export default Contact;