import React from "react";
import { Link } from "react-router-dom";
import "../styles/Home.css";

function Home() {
    return (
        <div className="home" >
            <div className="headerContainer">
                <h1> Cool Air, Cool Life</h1>
                <p>Cooling your Future with Technology </p>

                <Link to="/menu">
                    <button className="bot1"> Sales</button>
                </Link>
                <div>
                    <Link to="/Service">
                        <button className="bot2"> Service</button>
                    </Link>
                </div>
            </div>
        </div>
    );
}

export default Home;