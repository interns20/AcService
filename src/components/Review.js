import { Carousel } from 'antd';
import { useContext } from 'react';
import { Rate } from 'antd';
import { UserAuthenticationContext } from '../pages/Context/UserDetailsContext'
const contentStyle = {
    height: '160px',
    color: 'black',
    lineHeight: '100px',
    textAlign: 'center',
    background: 'lightblue',
};

const Review = () => {
    const { userInformation } = useContext(UserAuthenticationContext)
    return <Carousel autoplay>
        {userInformation && userInformation.body.feedback.map(e => <div key={e + Math.random()}>
            <Rate value={e.ratingss} />
            <h3 style={contentStyle}>{e.feedback}</h3>
        </div>)}

    </Carousel>
}

export default Review;