import React from "react";
import InstagramIcon from "@material-ui/icons/Instagram";
import TwitterIcon from "@material-ui/icons/Twitter";
import FacebookIcon from "@material-ui/icons/Facebook";
import YouTubeIcon from "@material-ui/icons/YouTube";
import "../styles/Footer.css";
function Footer() {
  return (
    <div className="footer">
      <div className="socialMedia">
        <a href="https://www.facebook.com/">
          <FacebookIcon />
        </a>
        <a href="https://www.instagram.com/">
          <InstagramIcon />
        </a>
        <a href="https://www.youtube.com">
          <YouTubeIcon />
        </a>
        <a href="https://twitter.com/">
          <TwitterIcon />
        </a>


      </div>
      <div className="foot">
        <p>  www.AIRorzo.org</p>
      </div>
    </div>
  );
}

export default Footer;
