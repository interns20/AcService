import React, { useState } from "react";
import { Input, Form, Button, Checkbox, Select, message } from 'antd'
import { Modal } from 'antd'




function MenuItem({ MenuItem }) {
  const [form] = Form.useForm();
  const { TextArea } = Input;
  const PRIMARY_API = "https://pnre7lwmvk.execute-api.ap-south-1.amazonaws.com/dev"
  const [isModalVisible, setIsModalVisible] = useState(false)

  const handleOk = () => {
    setIsModalVisible(false)
  }
  const onFinish = (values) => {
    form.resetFields()
    values.company = MenuItem.name
    const PRIMARY_API = "https://pnre7lwmvk.execute-api.ap-south-1.amazonaws.com/dev"
    var adminVerifyRaw = JSON.stringify({ "appName": "INTERNS_APP_DEV", "modName": "CREATE_APPOINTMENT", attributeName: "Booking", attributeValue: [values] })

    var adminVerifyRequestOptions = {

      method: 'POST',

      headers: { "Content-Type": "application/json" },

      body: adminVerifyRaw,

      redirect: 'follow'

    };

    fetch(`${PRIMARY_API}/general`, adminVerifyRequestOptions)

      .then(response => response.json())

      .then(result => {
        console.log(result)
        if (result.statusCode === 200) {

          message.success("Booking submitted successfully")
        }
      })
      .catch(err => console.log(err))

  }

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };
  return (
    <div className="menuItem">
      <div style={{ backgroundImage: `url(${MenuItem.image})` }}> </div>
      <h1> {MenuItem.name} </h1>
      <p> Rs:{MenuItem.price} </p>
      <button onClick={() => setIsModalVisible(true)}>Book Now</button>
      <Modal title={MenuItem.name} visible={isModalVisible} onOk={handleOk} onCancel={() => setIsModalVisible(false)}>

        <p>Power {MenuItem.Power}</p>
        <p>Refrigerant {MenuItem.Refrigerant}</p>
        <p>Temperature {MenuItem.Temperature}</p>


        <Form
          form={form}
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >

          <div className="text-start">Phone</div>
          <Form.Item

            name="Phone"
            rules={[{ required: true, message: 'Please input your Phone!' }]}
          >
            <Input type="number" placeholder="Enter Phone..." />
          </Form.Item>
          <div className="text-start">Email</div>
          <Form.Item

            name="Email"
            rules={[{ required: true, type: "email", message: 'Please input your Email!' }]}
          >
            <Input placeholder="Enter email..." />
          </Form.Item>

          <div className="text-start">Address</div>
          <Form.Item
            name="Address"
            rules={[{ required: true, message: 'Please input your Address!' }]}
          >
            <TextArea style={{ width: "200px" }} placeholder="Enter your Address " />
          </Form.Item>






          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button type="primary" htmlType="submit">
              submit
            </Button>
          </Form.Item>
        </Form>

      </Modal>
    </div >
  );
}

export default MenuItem;