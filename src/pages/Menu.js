import React from "react";
import { MenuList } from "../helpers/MenuList";
import MenuItem from "../components/MenuItem";
import "../styles/Menu.css";

function Menu() {
  return (
    <div className="menu">
      <h1 className="menuTitle"></h1>
      <div className="menuList">
        {console.log(MenuList)}
        {MenuList.map((menuItem, key) => {
          return (
            <MenuItem
              key={key}
              MenuItem={menuItem}
            />
          );
        })}
      </div>
    </div>
  );
}

export default Menu;


