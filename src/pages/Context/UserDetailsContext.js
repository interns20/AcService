import React, { createContext, useEffect, useState } from 'react'
import { Spin } from 'antd'
export const UserAuthenticationContext = createContext()
export default function UserAuthenticationContextProvider({ children }) {
    const [accessToken, setAccessToken] = useState(localStorage.getItem('accessToken') || '')
    const [userInformation, setUserInformation] = useState({})
    const PRIMARY_API = 'https://pnre7lwmvk.execute-api.ap-south-1.amazonaws.com/dev'
    const PRIMARY_APP_NAME = "INTERNS_APP_DEV"
    // const [accessToken,setAccessToken]=useState('eyJraWQiOiJ3bHowSm10eVdVbnA0U3ZneVwveDBOZFg0NzNiUjBDKzZCXC91c2U4NGk3S0U9IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiI5OWYwZDJkMy0xZjg2LTQzYjEtYmQ4OS04NGU3Y2MyMjE5NjgiLCJldmVudF9pZCI6Ijg0YzYxY2UyLWFlN2YtNDE0YS05OWZlLTRiZjBjZmQ1MzAyZCIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoiYXdzLmNvZ25pdG8uc2lnbmluLnVzZXIuYWRtaW4iLCJhdXRoX3RpbWUiOjE2NTMwNDYyMjEsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5hcC1zb3V0aC0xLmFtYXpvbmF3cy5jb21cL2FwLXNvdXRoLTFfTFRqQklMRlk3IiwiZXhwIjoxNjUzMDQ5ODIxLCJpYXQiOjE2NTMwNDYyMjEsImp0aSI6IjcwMDllNGFmLTY5NDUtNGI4NC04ZmJmLTE4N2JiM2QyNDE5MCIsImNsaWVudF9pZCI6IjcwdXJ2dDk4aGQ5Zm03cmg3MTRnaWhtZmVoIiwidXNlcm5hbWUiOiJqYXlzb25maW5ueTkzX2dtYWlsLmNvbSJ9.VVJ7rAIOGjcQfKQl3FyJDwi-6XkxF9ecgRmlBY9h-lHa2nofUP5oKgCEuiy1sYIj44trxkCenAnFprY_lh5LR9nHdSx9Ts1Tns9UuZ_xSZ8z0EuV2z3c6sFUZ9R_-LEV0AaDLLLnmwnl6Z0xON8lxzeFDhesvhr3GjWPR9cL998NE1Mx86iD3SZqYV-bz8IXIXngmZFsib5ZMjnvc7pVho7JrQ4wrbkXp8QcD-1d4pl1lVkN1nrlfWUtK7apbXPehMu-6dT-cPk6Xy7j6a59z6bmmLvAsbuRC0pi7YarOOh3rvekW9VMmM6E6kZrGa3zKggxahaEIMjDGGiO9Cydag')
    const [userLoading, setUserLoading] = useState(true)
    useEffect(() => {
        setUserLoading(true)
        var adminVerifyRaw = JSON.stringify({ "appName": PRIMARY_APP_NAME, "modName": "GET_DATA_INTERNS_AC", "elementId": "ac-service-form", "attributeName": "Bookings" })
        var adminVerifyRequestOptions = {
            method: 'POST',
            headers: { "Content-Type": "application/json", },
            body: adminVerifyRaw,
            redirect: 'follow'
        };
        fetch(`${PRIMARY_API}/general`, adminVerifyRequestOptions)
            .then(response => response.json())
            .then(result => {
                if (result.statusCode === 200) {
                    setUserInformation(result)
                    setUserLoading(false)
                }
            })
            .catch(e => { console.log(e) })
    }, [])
    if (userLoading)
        return <div className="text-center"><Spin /></div>
    return (
        <UserAuthenticationContext.Provider
            value={{
                accessToken, userInformation
            }}
        >
            {children}
        </UserAuthenticationContext.Provider>
    )
}
