import React from "react";
import { Input, Form, Button, Checkbox, Select, message } from 'antd'
import "./Service_request.css"
function Contact() {
  const [form] = Form.useForm();
  const { TextArea } = Input;
  const { Option } = Select;
  const PRIMARY_API = "https://pnre7lwmvk.execute-api.ap-south-1.amazonaws.com/dev"
  const onFinish = (values) => {
    form.resetFields()

    var adminVerifyRaw = JSON.stringify({ "appName": "INTERNS_APP_DEV", "modName": "CREATE_APPOINTMENT", attributeName: "details", attributeValue: [values] })

    var adminVerifyRequestOptions = {

      method: 'POST',

      headers: { "Content-Type": "application/json" },

      body: adminVerifyRaw,

      redirect: 'follow'

    };

    fetch(`${PRIMARY_API}/general`, adminVerifyRequestOptions)

      .then(response => response.json())

      .then(result => {
        console.log(result)
        if (result.statusCode === 200) {
          message.success("Appointment submitted successfully")
        }
      })
      .catch(err => console.log(err))
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };
  function handleChange(value) {
    console.log(`selected ${value}`);
  }
  return (
    <div className="service-form">
      <div className="form-container">


    
      <Form
        form={form}
        name="basic"
        
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        {/* <div><h1>Ac Service</h1></div> */}
        <div className="acc">Ac Service</div>
        <div className="text-start">Phone</div>
        <Form.Item

          name="Phone"
          rules={[{ required: true, message: 'Please input your Phone!' }]}
        >
          <Input type="number" placeholder="Enter Phone..." />
        </Form.Item>
        <div className="text-start">Email</div>
        <Form.Item

          name="Email"
          rules={[{ required: true, type: "email", message: 'Please input your Email!' }]}
        >
          <Input placeholder="Enter email..." />
        </Form.Item>
        <div className="text-start">Brand</div>

        <Form.Item
          name="Brand"
          rules={[{ required: true, message: 'Please select your Brand!' }]}
        >
          <Select placeholder="Select" onChange={handleChange}>
            <Option value="BlueStar">BlueStar</Option>
            <Option value="Samsung">Samsung</Option>
            <Option value="LG" >LG</Option>
            <Option value="Whirpool">Whirpool</Option>
            <Option value="Lloyd">Lloyd</Option>
            <Option value="Haier">Haier</Option>
          </Select>

        </Form.Item>
        <div className="text-start">Model</div>
        <Form.Item
          name="Model"
          rules={[{ required: true, message: 'Please input your model!' }]}
        >
          <Input placeholder="Enter Model Number" />
        </Form.Item>
        <div className="text-start">Address</div>
        <Form.Item
          name="Address"
          rules={[{ required: true, message: 'Please input your Address!' }]}
        >
          <TextArea  placeholder="Enter your Address " />
        </Form.Item>
        <div className="text-start">Message</div>
        <Form.Item
          name="Message"
          rules={[{ required: true, message: 'Please input your Complaint/Fault !' }]}
        >
          <TextArea  placeholder="Enter your Complaint/Fault " />
        </Form.Item>




        <Form.Item >
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
      </div>
    </div>

  );

}
export default Contact;

